﻿using AssGameFramework.Model;
using AssGameFramework.StateMachines;
using AssGameFramework.Systems;
using AssPong.StateMachines;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.StateSystems
{
    class GameLogicSystem : AbstractSystem
    {
        protected List<EntityModel> PlayerModels { get; private set; } = new List<EntityModel>();

        protected Queue<EntityModel> PlayersWaitingForSpawn { get; private set; } = new Queue<EntityModel>();

        protected Dictionary<int, List<Vector2>> SpawnLocations { get; private set; } = new Dictionary<int, List<Vector2>>();

        public float NextSpawn { get; set; } = 0.0f;

        public bool SpawnAll { get; set; } = false;

        private int Team = 0;

        public void GenerateSpawnPoints(Vector2 aScreenSize)
        {
            SpawnLocations.Clear();
            // Setup the spawn points for team 1 and 2
            for(int team = 0; team < 2; ++team)
            {
                SpawnLocations[team] = new List<Vector2>();

                SpawnLocations[team].Add(aScreenSize * new Vector2(0.25f, -0.5f) + (aScreenSize * team * new Vector2(0.5f, 0.0f)));
            }
        }

        public void RegisterPlayer(EntityModel model)
        {
            PlayerModels.Add(model);
        }

        public override void FixedUpdate(float step)
        {
            if (NextSpawn > 0.0f || SpawnAll)
            {
                NextSpawn -= step;

                if(NextSpawn <= 0.0f || SpawnAll)
                {
                    EntityModel model = PlayersWaitingForSpawn.Dequeue();

                    model.AddStateEvent<ShipStates, ShipEvents>(StateMachines.ShipEvents.RESPAWN, new EventAttributeMap( "location", SpawnLocations[Team][0]));
                    Team = (Team + 1) % 2;

                    model.Entity.Enabled = true;

                    if (PlayersWaitingForSpawn.Count > 0)
                    {
                        NextSpawn += 5.0f;
                    }
                    else
                    {
                        NextSpawn = -1.0f;
                        SpawnAll = false;
                    }
                }
            }
        }

        public override void StandardUpdate(float dt)
        {
            
        }

        internal void RegisterPlayerDeath(EntityModel model)
        {
            if(PlayersWaitingForSpawn.Count == 0)
            {
                NextSpawn = 5.0f;
            }

            PlayersWaitingForSpawn.Enqueue(model);

            if (PlayersWaitingForSpawn.Count == PlayerModels.Count)
            {
                SpawnAll = true;
            }
        }
    }
}
