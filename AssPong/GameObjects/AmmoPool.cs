﻿using AssGameFramework.Components;
using AssGameFramework.SceneGraph;
using AssGameFramework.Source.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.GameObjects
{
    public class AmmoPool
    {
        public delegate Entity AmmoFactoryDelegate();

        public uint Size { get; private set; } = 0U;

        protected Entity[] _ammoPool = null;
        protected DateTime[] _fireTime = null;

        public AmmoPool(uint aSize, AmmoFactoryDelegate ammoDelegate)
        {
            Size = aSize;
            _ammoPool = new Entity[aSize];
            _fireTime = new DateTime[aSize];

            for (int i = 0; i < aSize; ++i)
            {
                _ammoPool[i] = ammoDelegate();
                _ammoPool[i].Enabled = false;
                _fireTime[i] = DateTime.MinValue;
            }   
        }

        public Entity GetNextShot()
        {
            DateTime longestTime = DateTime.MaxValue;
            int longestIndex = -1;

            for(uint i = 0U; i < _ammoPool.Length; ++i)
            {
                Entity ent = _ammoPool[i];
                if (!ent.Enabled)
                {
                    // Entity isn't in use, use this
                    _fireTime[i] = DateTime.Now;
                    ent.Model.GetModelPart(out PhysicsModelPart physicsModel);
                    physicsModel.PhysicsBody.ResetDynamics();
                    return ent;
                }
                else
                {
                    // Track the longest surviving bullet in case there's no disabled entity
                    if(_fireTime[i] < longestTime)
                    {
                        longestIndex = (int)i;
                        longestTime = _fireTime[i];
                    }
                }
            }

            Debug.Assert(longestIndex >= 0, "Longest index cannot be less than zero");

            // If we have no disabled entities, just use the oldest active bullet
            _fireTime[longestIndex] = DateTime.Now;
            _ammoPool[longestIndex].Model.GetModelPart(out PhysicsModelPart oldestPhysicsModel);
            oldestPhysicsModel.PhysicsBody.ResetDynamics();
            return _ammoPool[longestIndex];
        }
    }
}
