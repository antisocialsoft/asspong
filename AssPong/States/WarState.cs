﻿using AssGameFramework.Components;
using AssGameFramework.Core;
using AssGameFramework.SceneGraph;
using AssGameFramework.GameSystems;
using AssGameFramework.States;
using AssPong.Factories;
using AssPong.ModelComponents;
using AssPong.StateSystems;
using FarseerPhysics.DebugView;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Source.Model;

namespace AssPong.States
{
    internal class WarState : AbstractState
    {
        protected Entity _cameraEnt = null;
        protected DebugViewXNA _debugView = null;
        protected Vector2 ScreenSize = Vector2.Zero;

        public GameLogicSystem GLSystem { get; } = new GameLogicSystem();
        public PhysicsSystem PhysicsSystem { get; private set; } = null;

        public WarState(ApplicationContext context) : base(context)
        {
        }

        protected override void Create()
        {
            Entity cameraEnt = new Entity();
            _cameraEnt = cameraEnt;
            cameraEnt.Model.AddPart(new Camera2DModelPart());

            Vector2 screenSize = new Vector2(Context.GraphicsManager.PreferredBackBufferWidth, Context.GraphicsManager.PreferredBackBufferHeight);

            cameraEnt.Transform.LocalPosition3D = new Vector3(screenSize.X * 0.5f, screenSize.Y * 0.5f, 0.0f);
            cameraEnt.Transform.LocalScale = new Vector2(0.75f);

            StateScene.RootEntity.AddChildEntity(cameraEnt);

            screenSize *= 1.0f/0.75f;
            ScreenSize = screenSize;
            StateScene.GetSystem<GameLogicSystem>().GenerateSpawnPoints(screenSize);

            ShipControlModelPart.ControlSetup controls = new ShipControlModelPart.ControlSetup();
            controls.BoostForward = Keys.W;
            controls.BoostBackwards = Keys.S;
            controls.TurnLeft = Keys.A;
            controls.TurnRight = Keys.D;
            controls.Fire = Keys.Q;

            Entity ship = ShipFactory.CreatePlayerControlledFighter(0, Context.Content, PhysicsSystem, GLSystem, Context.GetApplicationSystem<InputSystem>(), controls);

            StateScene.RootEntity.AddChildEntity(ship);
            ship.Transform.LocalPosition3D = new Vector3(screenSize.X * 0.5f, screenSize.Y * 0.5f, 0.0f);

            controls = new ShipControlModelPart.ControlSetup();
            controls.BoostForward = Keys.Up;
            controls.BoostBackwards = Keys.Down;
            controls.TurnLeft = Keys.Left;
            controls.TurnRight = Keys.Right;
            controls.Fire = Keys.RightControl;

            ship = ShipFactory.CreatePlayerControlledFighter(1, Context.Content, PhysicsSystem, GLSystem, Context.GetApplicationSystem<InputSystem>(), controls);

            StateScene.RootEntity.AddChildEntity(ship);
            ship.Transform.LocalPosition3D = new Vector3(screenSize.X * 0.5f, screenSize.Y * 0.4f, 0.0f);
        }

        protected override void PopulateSceneWithSystems(Scene toPopulate, ComponentSystem compSystem)
        {
            PhysicsSystem = new PhysicsSystem(new Vector2(0.0f, 0.0f), compSystem);
            toPopulate.AddSystem(PhysicsSystem);

            toPopulate.AddSystem(GLSystem);

            //Give your world to debugView
            _debugView = new DebugViewXNA(PhysicsSystem.PhysicsWorld);

            //add your graphicsDevice & your ContentManager
            _debugView.LoadContent(Context.GraphicsManager.GraphicsDevice, Context.Content);
        }

        protected override void Destroy()
        {
            
        }

        protected override void OnActive()
        {
            AudioSystem audioSys = Context.GetApplicationSystem<AudioSystem>();
            uint songID = audioSys.LoadSong("Audio/Grave_pong");
            audioSys.PlaySong(songID);
        }

        protected override void OnInactive()
        {
        }

        public override void LateFixedUpdate(float fixedStep)
        {
            base.LateFixedUpdate(fixedStep);

            //Check for any entities outside of the screen bounds and loop them around
            foreach (Entity ent in StateScene.RootEntity.Children)
            {
                Vector2 newPosition = ent.Transform.LocalPosition;
                float leftDistance = newPosition.X - (-ScreenSize.X * 0.005f);
                bool positionChanged = false;

                if (leftDistance < 0.0f)
                {
                    // Off the left side, correct
                    newPosition.X = (ScreenSize.X * 1.005f) + leftDistance;
                    positionChanged = true;
                }
                else
                {
                    float rightDistance = newPosition.X - (ScreenSize.X * 1.005f);
                    if (rightDistance > 0.0f)
                    {
                        newPosition.X = (-ScreenSize.X * 0.005f) + rightDistance;
                        positionChanged = true;
                    }
                }

                float topDistance = newPosition.Y - (-ScreenSize.Y * 0.008f);
                if (topDistance < 0.0f)
                {
                    newPosition.Y = ScreenSize.Y * 1.008f + topDistance;
                    positionChanged = true;
                }
                else
                {
                    float bottomDistance = newPosition.Y - (ScreenSize.Y * 1.008f);
                    if (bottomDistance > 0.0f)
                    {
                        newPosition.Y = (-ScreenSize.Y * 0.008f) + bottomDistance;
                        positionChanged = true;
                    }
                }
                if (positionChanged)
                {
                    ent.Transform.LocalPosition = newPosition;
                }
            }
        }

        public override void Draw(float deltaTime, SpriteBatch spriteBatch)
        {
            base.Draw(deltaTime, spriteBatch);

            float width = (Context.GraphicsManager.PreferredBackBufferWidth * (1.0f / _cameraEnt.Transform.WorldScale.X) * PhysicsSystem.SPRITE_TO_WORLD_SCALE);
            float height = (Context.GraphicsManager.PreferredBackBufferHeight * (1.0f / _cameraEnt.Transform.WorldScale.Y) * PhysicsSystem.SPRITE_TO_WORLD_SCALE);
            Vector2 cameraPos = _cameraEnt.Transform.WorldPosition * PhysicsSystem.SPRITE_TO_WORLD_SCALE * (1.0f / _cameraEnt.Transform.WorldScale.X);
            Matrix projection = Matrix.CreateOrthographicOffCenter((cameraPos.X) - (width / 2.0f),
                                                                (cameraPos.X) + (width / 2.0f),
                                                                (cameraPos.Y) + (height / 2.0f),
                                                                (cameraPos.Y) - (height / 2.0f),
                                                                0.1f, 50.0f);

            //Matrix viewMatrix = _cameraEnt.GetComponent<Camera2DComponent>().ViewMatrix;

            _cameraEnt.Model.GetModelPart(out Camera2DModelPart cameraModel);
            Matrix view = cameraModel.CameraComp.ViewMatrix;

            Matrix scale = Matrix.CreateScale(PhysicsSystem.SPRITE_TO_WORLD_SCALE);

            view = view * scale;

            // that ready see result
            //_debugView.RenderDebugData(ref projection);
        }
    }
}
