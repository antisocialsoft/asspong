﻿using AssGameFramework.Components;
using AssGameFramework.SceneGraph;
using AssGameFramework.Source.Model;
using AssGameFramework.StateMachines;
using AssPong.ModelComponents;
using AssPong.StateMachines;
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.Components
{
    public class CrashDetectionComponent : CollisionComponent
    {
        public PhysicsModelPart PhysicsModelPart { get; protected set; } = null;

        public CrashDetectionComponent(PhysicsModelPart physMdlComp) : base(physMdlComp.PhysicsComponent)
        {
            PhysicsModelPart = physMdlComp;
        }

        public override void ContactSolved(Entity collidedWith, Contact contact, ContactVelocityConstraint impulse)
        {
            base.ContactSolved(collidedWith, contact, impulse);

            HashSet<Contact> contacts = CollidedEntityLookup[collidedWith];

            float forceTotal = 0.0f;

            for(int i = 0; i < impulse.pointCount; ++i)
            {
                forceTotal += impulse.points[i].normalImpulse;
            }

            EventAttributeMap attributeMap = new EventAttributeMap();
            attributeMap["health"] = forceTotal;
            PhysicsModelPart.Model.AddStateEvent<ShipStates, ShipEvents>(ShipEvents.HEALTH_LOST, attributeMap);
        }
    }
}
