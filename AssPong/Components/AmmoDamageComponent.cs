﻿using AssGameFramework.Components;
using AssGameFramework.Core;
using AssGameFramework.SceneGraph;
using AssGameFramework.GameSystems;
using AssPong.ModelComponents;
using AssPong.StateMachines;
using FarseerPhysics.Dynamics.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Model;
using AssGameFramework.Source.Model;
using AssGameFramework.StateMachines;

namespace AssPong.Components
{
    internal class AmmoDamageComponent : CollisionComponent
    {
        public PhysicsModelPart PhysicsModelPart { get; protected set; } = null;

        public float BulletDamage { get; protected set; } = 1.0f;

        public AmmoDamageComponent(float damage, PhysicsModelPart physMdlComp) : base(physMdlComp.PhysicsComponent)
        {
            BulletDamage = damage;
            PhysicsModelPart = physMdlComp;
        }

        public override void ContactSolved(Entity collidedWith, Contact contact, ContactVelocityConstraint impulse)
        {
            base.ContactSolved(collidedWith, contact, impulse);

            EntityModel model = collidedWith.Model;

            if (model != null)
            {
                EventAttributeMap damageAttributeMap = new EventAttributeMap();
                damageAttributeMap["health"] = BulletDamage;
                model.AddStateEvent<ShipStates, ShipEvents>(ShipEvents.HEALTH_LOST, damageAttributeMap);
                AudioSystem audioSys = ApplicationContext.Instance.GetApplicationSystem<AudioSystem>();
                uint crashFX = audioSys.LoadSoundEffect("Audio/Lose Ball");
                audioSys.PlaySoundEffect(crashFX);
            }

            EventAttributeMap attributeMap = new EventAttributeMap();
            attributeMap["health"] = float.MaxValue;
            PhysicsModelPart.Model.AddStateEvent<ShipStates, ShipEvents>(ShipEvents.HEALTH_LOST, attributeMap);
        }

    }
}
