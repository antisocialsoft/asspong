﻿using AssGameFramework.Core;
using AssGameFramework.States;
using AssPong.States;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AssPong
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : AssGame
    {
        protected override AbstractState CreateInitialState()
        {
            return new WarState(Context);
        }
    }
}
