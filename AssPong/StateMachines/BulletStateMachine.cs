﻿using AssGameFramework.Model;
using AssGameFramework.StateMachines;
using AssGameFramework.Timers;
using AssPong.ModelComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.StateMachines
{
    internal class BulletStateMachine : StateMachineModel<ShipStates, ShipEvents, EntityModel>
    {
        public BulletStateMachine() : base(ShipStates.DEAD, ShipEvents.INSTANT)
        {
            AddStateTransition(ShipStates.SPAWNING, ShipStates.READY, ShipEvents.INSTANT);
            AddStateTransition(ShipStates.READY, ShipStates.DEAD, ShipEvents.HEALTH_LOST, BulletHitAction);
            AddStateTransition(ShipStates.DEAD, ShipStates.SPAWNING, ShipEvents.RESPAWN, SpawnAction);
            AddStateTransition(ShipStates.READY, ShipStates.SPAWNING, ShipEvents.RESPAWN, SpawnAction);
        }

        private bool SpawnAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            StandardTimer timer = model.GetModelComponent<BulletModelPart>().AliveTimer;
            timer.StartTimer(eventParams.GetValueAs<float>("LifeTime"));
            
            return true;
        }

        private bool BulletHitAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            model.Entity.Enabled = false;
            return true;
        }
    }
}
