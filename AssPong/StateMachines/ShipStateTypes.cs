﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.StateMachines
{
    public enum ShipStates
    {
        READY,
        DYING,
        DEAD,
        SPAWNING
    }

    public enum ShipEvents
    {
        INSTANT,
        BOOST_PRESSED,
        ATTACK_PRESSED,
        TURN_PRESSED,
        HEALTH_LOST,
        DYING_FINISHED,
        RESPAWN,
        ATTACK_RELEASED
    }

    public enum ShipGuards
    {
        HAS_AMMO,
        IS_DEAD,
        HAS_FUEL,
        IS_NOT_DEAD
    }
}
