﻿using AssGameFramework.StateMachines;
using AssPong.Components;
using AssPong.ModelComponents;
using AssPong.StateSystems;
using AssGameFramework.GameSystems;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Core;
using AssGameFramework.Model;
using AssGameFramework.Source.Model;

namespace AssPong.StateMachines
{
    internal class FighterStateMachine : StateMachineModel<ShipStates, ShipEvents, EntityModel>
    {

        public GameLogicSystem GameLogicSystem { get; protected set; } = null;

        protected Dictionary<ShipGuards, FighterStateMachine.GuardDelegate> _guardLookup = new Dictionary<ShipGuards, FighterStateMachine.GuardDelegate>();

        public FighterStateMachine(GameLogicSystem aGLS) : base(ShipStates.DEAD, ShipEvents.INSTANT)
        {
            GameLogicSystem = aGLS;
            InitialiseGuards();

            InitialiseReadyTransitions();

            InitialiseDeadTransitions();
        }

        private void InitialiseDeadTransitions()
        {
            AddStateTransition(ShipStates.READY, ShipStates.DYING, ShipEvents.HEALTH_LOST, DyingAction, _guardLookup[ShipGuards.IS_DEAD]);
            AddStateTransition(ShipStates.DYING, ShipStates.DEAD, ShipEvents.DYING_FINISHED, DeadAction);
            AddStateTransition(ShipStates.DEAD, ShipStates.SPAWNING, ShipEvents.RESPAWN, RespawnAction);
            AddStateTransition(ShipStates.SPAWNING, ShipStates.READY, ShipEvents.INSTANT, SpawnAction);
        }

        private bool SpawnAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            model.GetModelPart(out PhysicsModelPart physicsModel);
            physicsModel.PhysicsBody.ResetDynamics();
            model.GetModelComponent<ShipMovementModelPart>().Reset();

            AudioSystem audioSys = ApplicationContext.Instance.GetApplicationSystem<AudioSystem>();
            uint crashFX = audioSys.LoadSoundEffect("Audio/Refresh Game");
            audioSys.PlaySoundEffect(crashFX);

            return true;
        }

        private bool RespawnAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            //Reset health
            model.GetModelPart(out HealthModelPart healthModel);
            healthModel.Reset();
            model.Entity.Transform.LocalPosition = eventParams.GetValueAs<Vector2>("location");
            model.Entity.Transform.LocalRotation = 0.0f;

            return true;
        }

        private bool DeadAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            Debug.Print("DEAD! " + new TimeSpan(DateTime.Now.Ticks).TotalSeconds);
            model.GetModelPart(out PhysicsModelPart physicsModel);
            physicsModel.PhysicsBody.ResetDynamics();

            model.Entity.Enabled = false;
            GameLogicSystem.RegisterPlayerDeath(model);

            AudioSystem audioSys = ApplicationContext.Instance.GetApplicationSystem<AudioSystem>();
            uint crashFX = audioSys.LoadSoundEffect("Audio/Block Break");
            audioSys.PlaySoundEffect(crashFX);

            return true;
        }

        private bool DyingAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            Debug.Print("DYING! " + new TimeSpan(DateTime.Now.Ticks).TotalSeconds);
            model.GetModelPart<HealthModelPart>(out HealthModelPart HealthMComp);
            // Health model component will handle the actual dying part of dying
            HealthMComp.StartDying();

            return true;
        }

        private void InitialiseReadyTransitions()
        {
            AddStateTransition(ShipStates.READY, ShipStates.READY, ShipEvents.HEALTH_LOST, HitAction, _guardLookup[ShipGuards.IS_NOT_DEAD]);
            AddStateTransition(ShipStates.READY, ShipStates.READY, ShipEvents.ATTACK_PRESSED, AttackAction, _guardLookup[ShipGuards.HAS_AMMO]);
            AddStateTransition(ShipStates.READY, ShipStates.READY, ShipEvents.ATTACK_RELEASED, StopAttackAction);
            AddStateTransition(ShipStates.READY, ShipStates.READY, ShipEvents.BOOST_PRESSED, BoostAction, _guardLookup[ShipGuards.HAS_FUEL]);
            AddStateTransition(ShipStates.READY, ShipStates.READY, ShipEvents.TURN_PRESSED, TurnAction);
        }

        private bool StopAttackAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            Debug.Print("Stoppy stoppy");
            model.GetModelComponent<WeaponModelPart>().StopFiring();
            return true;
        }

        private bool HitAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            model.GetModelPart<HealthModelPart>(out HealthModelPart HealthMComp);
            HealthMComp.Health -= eventParams.GetValueAs<float>("health");

            return true;
        }

        private bool TurnAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            model.GetModelComponent<ShipMovementModelPart>().Turn(eventParams.GetValueAs<float>("dir"));
            return true;
        }

        private bool BoostAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            model.GetModelComponent<ShipMovementModelPart>().Boost(eventParams.GetValueAs<float>("dir"));
            return true;
        }

        private bool AttackAction(EntityModel model, ShipEvents transitionEvent, EventAttributeMap eventParams)
        {
            Debug.Print("Shooty shooty");
            model.GetModelComponent<WeaponModelPart>().Fire();
            return true;
        }

        private void InitialiseGuards()
        {
            _guardLookup = new Dictionary<ShipGuards, FighterStateMachine.GuardDelegate>
            {
                {
                    ShipGuards.HAS_AMMO,
                    (EntityModel model, ShipEvents evnt, EventAttributeMap eventParams) =>
                    { return  model.GetModelComponent<WeaponModelPart>().CanFire(); }
                },
                {
                    ShipGuards.HAS_FUEL,
                    (EntityModel model, ShipEvents evnt, EventAttributeMap eventParams) =>
                    { return true; }
                },
                {
                    ShipGuards.IS_DEAD,
                    (EntityModel model, ShipEvents evnt, EventAttributeMap eventParams) =>
                    { return model.GetModelPart<HealthModelPart>(out HealthModelPart healthComp) && healthComp.IsDead; }
                },
                {
                    ShipGuards.IS_NOT_DEAD,
                    (EntityModel model, ShipEvents evnt, EventAttributeMap eventParams) =>
                    { return !_guardLookup[ShipGuards.IS_DEAD](model, evnt, eventParams); }
                },
            };
        }
    }
}
