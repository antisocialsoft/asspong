﻿using AssGameFramework.Model;
using AssGameFramework.Source.Model;
using AssGameFramework.StateMachines;
using AssGameFramework.Timers;
using AssPong.Components;
using AssPong.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.ModelComponents
{
    class BulletModelPart : ModelPart
    {
        private AutoTimerHandler<StandardTimer> _bulletTimer { get; } = null;
        public StandardTimer AliveTimer
            {
             get { return _bulletTimer.ValueTimer; }
            set { _bulletTimer.ValueTimer = value; }
            }

        private AutoComponentHandler<AmmoDamageComponent> _AmmoDamComp = null;
        public AmmoDamageComponent AmmoDamageComp
        {
            get { return _AmmoDamComp; }
            set
            {
                if (_AmmoDamComp == null)
                {
                    _AmmoDamComp = new AutoComponentHandler<AmmoDamageComponent>(this, value);
                }
                else
                {
                    _AmmoDamComp.ValueComp = value;
                }
            }
        }

        public BulletModelPart()
        {
            _bulletTimer = new AutoTimerHandler<StandardTimer>(this, new StandardTimer());

            AliveTimer.TimerExpiredEvent += (StandardTimer expired) =>
            {
                EventAttributeMap attributeMap = new EventAttributeMap();
                attributeMap["health"] = float.MaxValue;
                this.Model.AddStateEvent<ShipStates, ShipEvents>(ShipEvents.HEALTH_LOST, attributeMap);
            };
        }
    }
}
