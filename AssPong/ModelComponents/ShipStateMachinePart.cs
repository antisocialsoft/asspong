﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Model;
using AssGameFramework.StateMachines;
using AssPong.StateMachines;

namespace AssPong.ModelComponents
{
    internal class ShipStateMachineModel : StateMachineModel<ShipStates, ShipEvents, EntityModel>
    {
        public ShipStateMachineModel(ShipStates entryState, ShipEvents instantEvent) : base(entryState, instantEvent)
        {
        }
    }

    internal class ShipStateMachineModelPart : StateMachineModelPart<ShipStates, ShipEvents>
    {
        public ShipStateMachineModelPart(ShipStateMachineModel.Instance stateMachine) : base(stateMachine)
        {
            StateMachine = stateMachine;
        }

        public void AddShipStateEvent(ShipEvents evnt, EventAttributeMap eventParams)
        {
            AddStateEvent(evnt, eventParams);
        }
    }
}
