﻿using AssGameFramework.Model;
using AssGameFramework.Source.Model;
using AssPong.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.ModelComponents
{
    public class AmmoPoolModelPart : ModelPart
    {
        public PhysicsModelPart PhysicsModel { get; set; } = null;

        public AmmoPool AmmoPool { get; set; } = null;

        public AmmoPoolModelPart(uint aAmmoPoolSize, AmmoPool.AmmoFactoryDelegate aAmmoDelegate, PhysicsModelPart aPhysicsModel)
        {
            AmmoPool = new AmmoPool(aAmmoPoolSize, aAmmoDelegate);
        }
    }
}
