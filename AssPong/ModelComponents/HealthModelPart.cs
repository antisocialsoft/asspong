﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Core;
using AssGameFramework.GameSystems;
using AssGameFramework.Model;
using AssGameFramework.Particles;
using AssGameFramework.SceneGraph;

namespace AssPong.ModelComponents
{
    internal class HealthModelPart : ModelPart
    {
        public float Health { get; set; } = 0.0f;
        public float MaxHealth { get; set; } = 0.0f;

        public bool IsDead { get { return Health < 0.0f; } }

        public BasicEmitterModelPart EmitterModel { get; protected set; } = null;

        protected int _dyingFX = -1;

        public HealthModelPart(float aMaxHealth, BasicEmitterModelPart aEmitterModel = null)
        {
            Health = aMaxHealth;
            MaxHealth = aMaxHealth;
            EmitterModel = aEmitterModel;
        }

        internal void StartDying()
        {
            //Register for an update to handle dying
            if (EmitterModel != null)
            {
                EmitterModel.EmitterComp.Enabled = true;
            }
            RequestUpdate(UpdateType.FIXED_UPDATE);

            AudioSystem audioSys = ApplicationContext.Instance.GetApplicationSystem<AudioSystem>();
            uint crashFX = audioSys.LoadSoundEffect("Audio/Refresh Game");
            _dyingFX = audioSys.PlaySoundEffect(crashFX, true, true, 0.25f, 0.25f);
        }

        protected override void FixedUpdate(float fixedStep)
        {
            Health += fixedStep * Health;

            if(Health <= -MaxHealth * 1.5f)
            {
                StopUpdate(UpdateType.FIXED_UPDATE);
                Model.AddStateEvent<StateMachines.ShipStates, StateMachines.ShipEvents>(StateMachines.ShipEvents.DYING_FINISHED, null);
                AudioSystem audioSys = ApplicationContext.Instance.GetApplicationSystem<AudioSystem>();
                uint crashFX = audioSys.LoadSoundEffect("Audio/Refresh Game");
                audioSys.StopSoundEffect(crashFX, _dyingFX);
            }
        }

        internal void Reset()
        {
            if (EmitterModel != null)
            {
                EmitterModel.EmitterComp.Enabled = false;
            }
            Health = MaxHealth;
        }
    }
}
