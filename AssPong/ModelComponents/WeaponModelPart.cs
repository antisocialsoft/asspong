﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Components;
using AssGameFramework.Core;
using AssGameFramework.SceneGraph;
using AssGameFramework.GameSystems;
using AssPong.GameObjects;
using Microsoft.Xna.Framework;
using AssGameFramework.Model;
using AssGameFramework.Source.Model;
using AssPong.StateMachines;
using AssGameFramework.StateMachines;

namespace AssPong.ModelComponents
{
    internal class WeaponModelPart : ModelPart
    {
        public class FiringModel
        {
            // Multiplier, 1 = same speed as ship, -1 = reverse speed, 0 = bullet uneffected by ship speed
            public float FireSpeedMultiplier { get; set; } = 0.1f;
            // Fires at specified speed, negative values fire backwards
            public float FireSpeed { get; set; } = 70.0f;
            // Fire rate, determines the time between shots in seconds
            public float FireRate { get; set; } = 0.25f;
            // Determines the burst rate, as in, how many shots should fire in a single 'burst'
            public uint BurstSize { get; set; } = 5U;
            // Determines how long the burst should last (NB: Does not increase fire rate)
            public float BurstSpeed { get; set; } = 1.0f;
            // Spread determines the angle that the bullet can deviate when fired (radians)
            public float Spread { get; set; } = 0.0f;
            // This determines the life time of the object in seconds (-1 is infinite)
            public float LifeTime { get; set; } = 1.5f;
            // This allows the gun to fire continuously until told to stop
            public bool AutoFire { get; set; } = true;

            // Convenience methods

            public float TimeBetweenBurstShots { get { return (BurstSpeed / (BurstSize - 1U)); } }
        }

        public AmmoPoolModelPart AmmoPoolModel { get; protected set; } = null;

        public FiringModel FiringMode { get; protected set; } = null;

        public DateTime LastShot { get; protected set; } = DateTime.MinValue;

        public float TimeUntilNextBurstShot { get; protected set; } = 0.0f;

        public uint BurstShotsToFire { get; protected set; } = 0U;

        public TeamModelPart TeamModel { get; protected set; } = null;

        public WeaponModelPart(TeamModelPart aTeamModel, FiringModel aFiringModel, AmmoPoolModelPart aAmmoPoolModel)
        {
            FiringMode = aFiringModel;
            AmmoPoolModel = aAmmoPoolModel;
            TeamModel = aTeamModel;
        }

        internal delegate void OnShotFiredDelegate(Entity shot, Vector2 fireVelocity);

        public void Fire(OnShotFiredDelegate shotFiredDelegate = null)
        {
            Debug.Assert(CanFire(), "Attempt to fire faster than allowed");

            FireBurst(shotFiredDelegate);
        }

        protected void FireBurst(OnShotFiredDelegate shotFiredDelegate)
        {
            LastShot = DateTime.Now;

            // Determine speed of bullet
            Model.GetModelPart<PhysicsModelPart>(out PhysicsModelPart physModel);

            Vector2 fireVelocity = Vector2.Zero;

            Vector2 shipHeading = Vector2.Transform(-Vector2.UnitY, Matrix.CreateRotationZ(physModel.PhysicsBody.Rotation));

            if (FiringMode.FireSpeedMultiplier != 0)
            {
                Vector2 shipSpeed = physModel.PhysicsBody.LinearVelocity;
                fireVelocity = shipSpeed * FiringMode.FireSpeedMultiplier;
            }

            fireVelocity += shipHeading * FiringMode.FireSpeed;

            uint shotsToFire = 1U;

            // If we're in the middle of a burst, don't process the fire mode
            if (BurstShotsToFire == 0)
            {
                if(FiringMode.AutoFire)
                {
                    // Autofire
                    TimeUntilNextBurstShot = FiringMode.TimeBetweenBurstShots;
                    BurstShotsToFire = 1;
                }
                if (FiringMode.BurstSpeed > 0.0f)
                {
                    // Burst mode
                    TimeUntilNextBurstShot = FiringMode.TimeBetweenBurstShots;
                    BurstShotsToFire = FiringMode.BurstSize - 1U;

                    //Register for an update
                    RequestUpdate(UpdateType.FIXED_UPDATE);
                }
                else
                {
                    // Shoot everything at once
                    shotsToFire = FiringMode.BurstSize;
                }
            }

            Random random = new Random(new TimeSpan(DateTime.Now.Ticks).Milliseconds);

            for (uint i = 0U; i < shotsToFire; ++i)
            {
                //Adjust for spread, if set
                if (FiringMode.Spread != 0.0f)
                {
                    float spreadRange = FiringMode.Spread * 2.0f;
                    //Calculate spread from -spread to +spread
                    float spread = (spreadRange * (float)random.NextDouble()) - FiringMode.Spread;
                    fireVelocity = Vector2.Transform(fireVelocity, Matrix.CreateRotationZ(spread));
                }

                Entity bullet = AmmoPoolModel.AmmoPool.GetNextShot();
                bullet.Enabled = true;
                //Reset bullet
                EventAttributeMap attributeMap = new EventAttributeMap();
                attributeMap["LifeTime"] = FiringMode.LifeTime;
                bullet.Model.AddStateEvent<ShipStates, ShipEvents>(StateMachines.ShipEvents.RESPAWN, attributeMap);

                if (bullet.SceneOwner == null)
                {
                    Model.Entity.SceneOwner.RootEntity.AddChildEntity(bullet);
                }
                bullet.Transform.LocalPosition = Model.Entity.Transform.WorldPosition;
                bullet.Transform.LocalRotation = (float)Math.Atan2(fireVelocity.X, -fireVelocity.Y);
                PhysicsComponent bulletPhysics = bullet.Model.GetModelComponent<PhysicsModelPart>().PhysicsComponent;
                Debug.Assert(!float.IsNaN(bulletPhysics.PhysicsBody.LinearVelocity.X));
                bulletPhysics.PhysicsBody.ResetDynamics();
                Debug.Assert(!float.IsNaN(bulletPhysics.PhysicsBody.LinearVelocity.X));
                bulletPhysics.PhysicsBody.LinearVelocity = fireVelocity;

                bulletPhysics.PhysicsBody.IgnoreCollisionWith(physModel.PhysicsBody);


                shotFiredDelegate?.Invoke(bullet, fireVelocity);
            }

            // Ideally wouldn't rely on the global usage here, but for convenience
            AudioSystem audioSys = ApplicationContext.Instance.GetApplicationSystem<AudioSystem>();

            if (TeamModel.TeamNumber % 2 == 1)
            {
                uint soundEffectHandle = audioSys.LoadSoundEffect("Audio/ping");
                audioSys.PlaySoundEffect(soundEffectHandle);
            }
            else
            {
                uint soundEffectHandle = audioSys.LoadSoundEffect("Audio/pong");
                audioSys.PlaySoundEffect(soundEffectHandle);
            }
        }

        internal void StopFiring()
        {
            //Only works with autofire
            if(FiringMode.AutoFire)
            {
                StopUpdate(UpdateType.FIXED_UPDATE);
                BurstShotsToFire = 0;
            }
        }

        internal bool CanFire()
        {
            return FiringMode.AutoFire || FireTimeExpired();
        }

        private bool FireTimeExpired()
        {
            return ((DateTime.Now - LastShot).TotalSeconds > FiringMode.FireRate);
        }

        protected override void FixedUpdate(float fixedStep)
        {
            TimeUntilNextBurstShot -= fixedStep;

            // Check fire time
            if (TimeUntilNextBurstShot < 0.0f)
            {
                // Time to fire
                uint shotsToFire = 1U + Math.Min(BurstShotsToFire - 1U, (uint)Math.Abs(TimeUntilNextBurstShot / FiringMode.TimeBetweenBurstShots));

                for (uint i = 0U; i < shotsToFire; ++i)
                {
                    float actualFireTime = 0.0f;
                    FireBurst((Entity bullet, Vector2 fireVelocity) =>
                    {
                        // Attempt to adjust for the time it should have fired
                        float lateTime = actualFireTime - TimeUntilNextBurstShot;
                        bullet.Transform.LocalPosition += fireVelocity * lateTime;
                        actualFireTime -= FiringMode.TimeBetweenBurstShots;
                    });

                    // If we've got autofire, then we never stop
                    if (!FiringMode.AutoFire)
                    {
                        BurstShotsToFire--;
                    }
                }
                
                // IF we've fired the burst, stop the updates
                if(BurstShotsToFire == 0)
                {
                    StopUpdate(UpdateType.FIXED_UPDATE);
                }

                TimeUntilNextBurstShot += FiringMode.TimeBetweenBurstShots * shotsToFire;
            }
        }
    }
}
