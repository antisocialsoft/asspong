﻿using AssGameFramework.Components;
using AssGameFramework.GameSystems;
using AssGameFramework.Model;
using AssGameFramework.Particles;
using AssGameFramework.Source.Model;
using AssPong.Components;
using AssPong.GameObjects;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AssPong.ModelComponents.ShipControlModelPart;
using static AssPong.ModelComponents.WeaponModelPart;

namespace AssPong.ModelComponents
{
    class ShipModelPart : ModelPart
    {
        #region ModelParts
        public PhysicsModelPart PhysicsModel { get; private set; } = null;
        public HealthModelPart HealthModel { get; private set; } = null;
        public ShipControlModelPart ShipControlModel { get; private set; } = null;
        public ShipMovementModelPart ShipMovementPart { get; private set; } = null;
        public TeamModelPart TeamModel { get; private set; } = null;
        public WeaponModelPart WeaponModel { get; private set; } = null;
        public BasicEmitterModelPart EmitterModel { get; private set; } = null;
        #endregion

        #region Components
        private AutoComponentHandler<CrashDetectionComponent> _crashDetectionComp = null;
        public CrashDetectionComponent CrashDetectionComp
        {
            get { return _crashDetectionComp; }
            set
            {
                if (_crashDetectionComp == null)
                {
                    _crashDetectionComp = new AutoComponentHandler<CrashDetectionComponent>(this, value);
                }
                else
                {
                    _crashDetectionComp.ValueComp = value;
                }
            }
        }
        #endregion

        public ShipModelPart(PhysicsModelPart physicsModel, int team,
            InputSystem inputSystem, ControlSetup controlScheme, 
            FiringModel firingModel, AmmoPoolModelPart ammoModel, Texture2D particleTex)
        {
            PhysicsModel        = physicsModel;

            BasicParticleAffectorFactory affectorFactory = CreateParticleAffectorFactory();
            Texture2DParticleFactory particleFactory = new Texture2DParticleFactory(particleTex, affectorFactory);

            EmitterModel = new BasicEmitterModelPart(100, 50, particleFactory, affectorFactory);

            HealthModel         = new HealthModelPart(50.0f, EmitterModel);
            ShipControlModel    = new ShipControlModelPart(inputSystem, controlScheme);
            ShipMovementPart    = new ShipMovementModelPart(PhysicsModel);
            TeamModel           = new TeamModelPart(team);
            WeaponModel         = new WeaponModelPart(TeamModel, firingModel, ammoModel);

            CrashDetectionComp = new CrashDetectionComponent(PhysicsModel);
        }

        public BasicParticleAffectorFactory CreateParticleAffectorFactory()
        {
            BasicParticleAffectorFactory affectFactory = new BasicParticleAffectorFactory();
            affectFactory.AngleRange = 0.5f;
            affectFactory.MinSpeed = 1.0f;
            affectFactory.SpeedRange = 3.0f;
            affectFactory.StartColour = Color.White;
            affectFactory.EndColour = Color.DarkSlateGray;
            affectFactory.MinLifeTime = 0.2f;
            affectFactory.LifeTimeRange = 0.6f;

            return affectFactory;
        }

        protected override void OnAttachedToModel()
        {
            Model.AddPart(HealthModel);
            Model.AddPart(ShipControlModel);
            Model.AddPart(ShipMovementPart);
            Model.AddPart(TeamModel);
            Model.AddPart(WeaponModel);
            Model.AddPart(EmitterModel);
        }

        protected override void OnRemovedFromModel()
        {
            Model.RemovePart(HealthModel);
            Model.RemovePart(ShipControlModel);
            Model.RemovePart(ShipMovementPart);
            Model.RemovePart(TeamModel);
            Model.RemovePart(WeaponModel);
            Model.RemovePart(EmitterModel);
        }
    }
}
