﻿
using AssGameFramework.Model;

namespace AssPong.ModelComponents
{
    public class TeamModelPart : ModelPart
    {
        public int TeamNumber { get; protected set; } = -1;

        public TeamModelPart(int teamNumber) : base()
        {
        }
    }
}