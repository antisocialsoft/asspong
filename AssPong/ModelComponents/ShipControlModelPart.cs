﻿using AssGameFramework.Components;
using AssGameFramework.GameSystems;
using AssGameFramework.Model;
using AssGameFramework.StateMachines;
using AssPong.ModelComponents;
using AssPong.StateMachines;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.ModelComponents
{
    internal class ShipControlModelPart : ModelPart
    {
        public class ControlSetup
            {
            public Keys BoostForward { get; set; }
            public Keys BoostBackwards { get; set; }
            public Keys TurnLeft { get; set; }
            public Keys TurnRight { get; set; }
            public Keys Fire { get; set; }
            }


        public InputSystem InputSystem { get; protected set; } = null;
        public Dictionary<Keys, Action<bool>> ControlScheme { get; protected set; } = new Dictionary<Keys, Action<bool>>();

        public ShipControlModelPart(InputSystem inputSystem, ControlSetup controlScheme)
        {
            InputSystem = inputSystem;

            InputSystem.SubscribeToKeyPressedEvent(OnKeyChanged);
            InputSystem.SubscribeToKeyReleasedEvent(OnKeyChanged);

            ControlScheme[controlScheme.BoostForward] =     (bool state) => Model.AddStateEvent<ShipStates, ShipEvents>(StateMachines.ShipEvents.BOOST_PRESSED, new EventAttributeMap ( "dir", (state) ? -1.0f : 0.0f ));
            ControlScheme[controlScheme.BoostBackwards] =   (bool state) => Model.AddStateEvent<ShipStates, ShipEvents>(StateMachines.ShipEvents.BOOST_PRESSED, new EventAttributeMap ( "dir", (state) ? 1.0f : 0.0f ));
            ControlScheme[controlScheme.TurnLeft] =         (bool state) => Model.AddStateEvent<ShipStates, ShipEvents>(StateMachines.ShipEvents.TURN_PRESSED, new EventAttributeMap ( "dir", (state) ? -1.0f : 0.0f ));
            ControlScheme[controlScheme.TurnRight] =        (bool state) => Model.AddStateEvent<ShipStates, ShipEvents>(StateMachines.ShipEvents.TURN_PRESSED, new EventAttributeMap ( "dir", (state)? 1.0f : 0.0f ));
            ControlScheme[controlScheme.Fire] =             (bool state) => Model.AddStateEvent<ShipStates, ShipEvents>((state)? StateMachines.ShipEvents.ATTACK_PRESSED : StateMachines.ShipEvents.ATTACK_RELEASED, new EventAttributeMap());
        }

        private void OnKeyChanged(Keys keyPressed, bool state)
        {
            Debug.Print(((state)? "Key Pressed: " : "Key Released: ") + keyPressed);
            if(ControlScheme.ContainsKey(keyPressed))
            {
                ControlScheme[keyPressed](state);
            }
        }
    }
}
