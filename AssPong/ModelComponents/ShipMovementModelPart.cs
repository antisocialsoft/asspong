﻿using AssGameFramework.Model;
using AssGameFramework.SceneGraph;
using AssGameFramework.Source.Model;
using AssPong.ModelComponents;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssPong.ModelComponents
{
    internal class ShipMovementModelPart : ModelPart
    {
        public PhysicsModelPart PhysicsModelPart { get; protected set; } = null;

        public float BoostDirection { get; set; } = 0.0f;
        public float TurnDirection { get; set; } = 0.0f;

        public ShipMovementModelPart(PhysicsModelPart physicsComp) : base()
        {
            PhysicsModelPart = physicsComp;
        }

        protected override void OnAttachedToModel()
        {
            base.OnAttachedToModel();

            RequestUpdate(UpdateType.FIXED_UPDATE);
        }

        protected override void OnRemovedFromModel()
        {
            base.OnRemovedFromModel();

            StopUpdate(UpdateType.FIXED_UPDATE);
        }

        public void Boost(float direction)
        {
            BoostDirection = direction;
        }

        public void Turn(float direction)
        {
            TurnDirection = direction;
        }

        public void Reset()
        {
            BoostDirection = 0.0f;
            TurnDirection = 0.0f;
        }

        protected override void FixedUpdate(float fixedStep)
        { 
            Vector2 heading = Vector2.Transform(Vector2.UnitY, Matrix.CreateRotationZ(PhysicsModelPart.PhysicsBody.Rotation));
            Debug.Print("Heading: " + heading.X + ", " + heading.Y);

            PhysicsModelPart.PhysicsBody.ApplyForce(new Vector2(100.0f, 100.0f) * heading * BoostDirection);

            PhysicsModelPart.PhysicsBody.ApplyTorque(14.0f * TurnDirection);
        }

        
    }
}
