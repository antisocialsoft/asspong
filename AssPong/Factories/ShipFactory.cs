﻿using AssGameFramework.Components;
using AssGameFramework.SceneGraph;
using AssGameFramework.GameSystems;
using AssGameFramework.Particles;
using AssGameFramework.Timers;
using AssPong.Components;
using AssPong.GameObjects;
using AssPong.ModelComponents;
using AssPong.StateMachines;
using AssPong.StateSystems;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssGameFramework.Model;
using AssGameFramework.Source.Model;

namespace AssPong.Factories
{
    class ShipFactory
    {
        internal static Entity CreatePlayerControlledFighter(int team, ContentManager content, PhysicsSystem physicsSystem, GameLogicSystem gls, InputSystem inputSystem, ShipControlModelPart.ControlSetup controlSetup)
        {
            Entity fighterEnt = CreateFighter(team, content, physicsSystem, gls, inputSystem, controlSetup);

            return fighterEnt;
        }

        public static Entity CreateFighter(int team, ContentManager content, PhysicsSystem physicsSystem, GameLogicSystem gls, InputSystem inputSystem, ShipControlModelPart.ControlSetup controlSetup)
        {
            Entity fighterEnt = new Entity();
            fighterEnt.Enabled = false;

            fighterEnt.Model.AddPart(new SpriteModelPart(content.Load<Texture2D>("Ships/fighter")));
            
            Vertices verts = new Vertices()
            {
                new Vector2(0.0f, -1.0f),
                new Vector2(-1.0f, 1.25f),
                new Vector2(1.0f, 1.25f)
            };

            Body shipBody = BodyFactory.CreatePolygon(physicsSystem.PhysicsWorld, verts, 1.0f);
            shipBody.BodyType = BodyType.Dynamic;

            shipBody.LinearDamping = 0.8f;
            shipBody.AngularDamping = 3.0f;

            shipBody.CollidesWith = Category.All;
            shipBody.CollisionCategories = Category.Cat2;

            PhysicsComponent shipPhysics = new PhysicsComponent(shipBody);

            gls.RegisterPlayer(fighterEnt.Model);
            gls.RegisterPlayerDeath(fighterEnt.Model);

            WeaponModelPart.FiringModel firingMode = new WeaponModelPart.FiringModel();

            AmmoPool.AmmoFactoryDelegate ammoDelegate = () =>
                {
                    return CreateFighterAmmo(content, physicsSystem);
                };

            PhysicsModelPart physicsModel = new PhysicsModelPart(shipPhysics);
            fighterEnt.Model.AddPart(physicsModel);
            AmmoPoolModelPart ammoModel = new AmmoPoolModelPart(6, ammoDelegate, physicsModel);
            fighterEnt.Model.AddPart(ammoModel);

            fighterEnt.Model.AddPart(new ShipModelPart(physicsModel, team, inputSystem, controlSetup, firingMode, ammoModel, content.Load<Texture2D>("Ships/flackshot")));

            FighterStateMachine stateMachine = new FighterStateMachine(gls);
            StateMachineModelPart<ShipStates, ShipEvents> stateModel = new StateMachineModelPart<ShipStates, ShipEvents>(stateMachine.CreateInstance(fighterEnt.Model));
            fighterEnt.Model.AddPart(stateModel);

            return fighterEnt;
        }

        private static Entity CreateFighterAmmo(ContentManager content, PhysicsSystem physicsSystem)
        {
            Entity bullet = new Entity();
            SpriteModelPart spriteModel = new SpriteModelPart(content.Load<Texture2D>("Ships/gunshot"));
            bullet.Model.AddPart(spriteModel);

            Body bulletBody = BodyFactory.CreateCircle(physicsSystem.PhysicsWorld, 0.5f, 0.05f);
            bulletBody.BodyType = BodyType.Dynamic;

            bulletBody.CollidesWith = Category.All & ~Category.Cat1;
            bulletBody.CollisionCategories = Category.Cat1;

            PhysicsComponent bulletPhysics = new PhysicsComponent(bulletBody);
            PhysicsModelPart physicsModel = new PhysicsModelPart(bulletPhysics);

            bullet.Model.AddPart(physicsModel);

            BulletModelPart bulletModel = new BulletModelPart();
            bullet.Model.AddPart(bulletModel);
            bulletModel.AmmoDamageComp = new AmmoDamageComponent(27.0f, physicsModel);

            BulletStateMachine bulletStateMachine = new BulletStateMachine();

            bullet.Model.AddPart(new StateMachineModelPart<ShipStates, ShipEvents>(bulletStateMachine.CreateInstance(bullet.Model)));

            bullet.Model.AddPart(new HealthModelPart(0.01f));

            return bullet;
        }
    }
}
